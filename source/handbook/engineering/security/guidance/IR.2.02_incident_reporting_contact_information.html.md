---
layout: markdown_page
title: "IR.2.02 - Incident Reporting Contact Information Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# IR.2.02 - Incident Reporting Contact Information

## Control Statement

GitLab provides a contact method for external parties to:

* Submit complaints and inquiries
* Report incidents

## Context

Having an easily accessible and public channel for external parties to contact GitLab in the event of a security incident provides a way for the community to help GitLab keep its systems safe and to faster identify and respond to security incidents internally.

## Scope

TBD

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IR.2.02_incident_reporting_contact_information.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IR.2.02_incident_reporting_contact_information.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IR.2.02_incident_reporting_contact_information.md).

## Framework Mapping

* ISO
  * A.16.1.2
* SOC2 CC
  * CC2.3
* PCI
  * 12.10.1
