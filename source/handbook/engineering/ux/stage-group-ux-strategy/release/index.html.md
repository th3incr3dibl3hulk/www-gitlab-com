---
layout: markdown_page
title: "Release UX"
---

### On this page

{:.no_toc}

- TOC
{:toc}

## Overview

Content coming soon!

### Our customer

Content coming soon!

### Our user
We have different user types we consider in our experience design effort. Even when a user has the same title, their responsibilities may vary by organization size, department, org structure, and role. Here are some of the people we are serving:

* [Software Developer](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)
* [Development Tech Lead](/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead) 
* [DevOps Engineer](/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)
* [Product Manager](/handbook/marketing/product-marketing/roles-personas/#parker-product-manager)
* QA

### Our baseline experience 

##### Primary Jobs to be done ([JTBD](https://gitlab.com/groups/gitlab-org/-/epics/1326))

* **Create a Release and update it:** When tracking important deliverables in my project, I want to easily create and manage release entries in GitLab, so I can provide packaged software, notes, and files for people to use.
     * Current walkthrough: [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/431)
     * Recent recommendation: *in progress*
* **Deploy to GitLab Pages:** When using a static site generator, I want automatic deployments every time I commit a change, so that I can keep my site up to date without manual builds.
     * Current walkthrough:  [view issue](https://gitlab.com/gitlab-org/gitlab-design/issues/412)
     * Recent recommendation: *in progress*

### Our team
Our team continues to grow. We currently have 4 members that contribute to Release UX efforts:

* [Valerie Karnes](https://gitlab.com/vkarnes) - UX Manager 
* [Rayana Verissimo](https://gitlab.com/rverissimo) - Sr. Product Designer
* [Mike Nichols](https://gitlab.com/mnichols1) - Sr. Product Designer
* [Lorie Whitaker](https://gitlab.com/loriewhitaker) - Sr. UX Researcher

#### Our team meetings

* Release PMs and UX weekly on Wednesdays at 09:30am EST
* Release Managers and Product Design weekly on Mondays at 09:30am EST. Meeting to collaborate as a cross-functional team on top Release topics that require speed or might be better served as a working session.
* CI/CD UX monthly meeting on Fridays at 10:30am EST. Meeting to discuss our stages UX shared efforts, review designs, and iterate on our strategy.
* Product Design does a bi-weekly grooming session on Tuesdays at 10:30pm EST

## Our Structure

We've divided the Release stage into dedicated experience groups to align with a similar [split](/handbook/product/categories/#release-stage) undertaken by our engineering and PM counterparts.

| Progressive Delivery group | Dedicated Designer |
|--|--|
[Continuous Delivery (CD)](/direction/release/continuous_delivery/) | Rayana Verissimo |
[Incremental Rollout](/direction/release/continuous_delivery/) | Mike Nichols |
[Review apps](/direction/release/review_apps/) | Shared |
[Feature Flags](/direction/release/feature_flags/) | Shared | 

| Release Management group | Dedicated Designer |
|--|--|
| [Release Orchestration](/direction/release/release_orchestration/) | Rayana Verissimo |
| Secrets Management | Mike Nichols | 
| Release Governance | Rayana Verissimo | 
| [Pages](/direction/release/pages/) | Mike Nichols | 

This segmentation gives us a better opportunity to:

* Grow our expertise and knowledge within our subgroup while sharing relevant information with the rest of the team.
* Evolve and maintain relationships with our dedicated engineering team and PMs.
* Serve as the known main point of contact.
* Deeply understand our users' needs by initiating and/or leading research activities specific to our experience group.
* Focus on iterating and progressing our experiences from MVC to Lovable.

## Our strategy

Coming soon!