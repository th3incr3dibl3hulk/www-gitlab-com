---
layout: markdown_page
title: "Solutions Architect Onboarding"
---

This page has been deprecated and added to the [People Ops onboarding issue template](https://about.gitlab.com/handbook/general-onboarding/#people-ops-onboarding-issue-template).